import Select from './Select.typeRenderer';
import Text from './Text.typeRenderer';
import Image from './Image.typeRenderer';
import Range from './Range.typeRenderer';
import Email from './Email.typeRenderer';
import Brand from './Brand.typeRenderer';
import TextArea from './TextArea.typeRenderer';

export default {
   select: Select,
   number: Text,
   text: Text,
   image: Image,
   range: Range,
   email: Email,
   brand: Brand,
   textarea: TextArea
};
