import React from 'react';
import PropTypes from 'prop-types';

import Text from './Text.typeRenderer';

/**
 * Email field renderer: Wrapper for text field to enable native browser email validation
 */
const Email = props => {
   return <Text {...props} />;
};

Email.propTypes = {
   field: PropTypes.object.isRequired,
   setter: PropTypes.func.isRequired,
   name: PropTypes.string.isRequired,
   value: PropTypes.string
};

export default Email;
