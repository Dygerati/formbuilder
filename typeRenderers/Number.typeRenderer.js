import React from 'react';
import PropTypes from 'prop-types';

import Text from './Text.typeRenderer';

/**
 * Number field renderer: Wrapper for text field to enable native number field function
 */
const Number = props => {
   return <Text {...props} />;
};

Number.propTypes = {
   field: PropTypes.object.isRequired,
   setter: PropTypes.func.isRequired,
   name: PropTypes.string.isRequired,
   value: PropTypes.string
};

export default Number;
