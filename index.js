
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormGroup, Label, Container } from 'reactstrap';

import SaveButton from '../components/SaveButton';
import styles from '../css/FormBuilder.module.css';
import typeRenderers from './typeRenderers';

/**
 * FormBuilder module: Build form with handler based on provided field config
 */
class FormBuilder extends Component {
   static propTypes = {
      fields: PropTypes.object.isRequired,
      afterSave: PropTypes.func,
      buttonDisplayText: PropTypes.string,
      submitEmitter: PropTypes.object,
      displaySaveBtn: PropTypes.bool
   };

   static defaultProps = {
      buttonDisplayText: '',
      displaySaveBtn: true
   };

   /**
    * Compare Handlers, function mapping providing method of determining whether a field has 
    * changed based on field type
    */
   compareHandlers = {
      image: (name, field) => {
         return field.mobxGetter() && this.state[name]
            ? field.mobxGetter().size === this.state[name].size
            : this.compareHandlers.default(name, field);
      },
      default: (name, field) => {
         return field.mobxGetter() === this.state[name];
      }
   };

   /**
    * Function to ultimately submit form, ensures validation and then initiates submission
    *
    * @param {Event} e - Optional event object for form submission
    */
   handleFormSubmit(e) {
      e && e.preventDefault();

      if (this.validateForm()) {
         this.submitFormData();
      }
   }

   /**
    * Simply checks validity of form using standard HTML utility methods
    *
    * @param {Event} e - Optional event object for form submission
    * @returns	{Boolean} - Inidicates whether form fields are valid
    */
   validateForm(e) {
      e && e.preventDefault();

      this.setState({
         validated: true
      });

      return this.form.checkValidity();
   }

   /**
    * Submits only changed fields using provided setters
    *
    * @param {Array} subset - Optional array to narrow to certain fields
    */
   async submitFormData(subset = []) {
      const { fields } = this.props;

      const selectFields = !subset.length
         ? fields
         : subset.reduce((obj, curr) => {
            obj[curr] = fields[curr];
            return obj;
         }, {});

      const queue = [];

      for (const [ name, field ] of Object.entries(selectFields)) {
         // Field not changed, don't save
         if (!this.changedFields.includes(name)) {
            continue;
         }

         // Add promise to queue
         queue.push(Promise.resolve(field.mobxSetter(this.state[name], this.state)));
      }

      // Wait for any queued saves
      await Promise.all(queue);

      if (typeof this.props.afterSave === 'function') {
         await this.props.afterSave();
      }
   }

   /**
    * Simple getter to return only fields that have changed
    */
   get changedFields() {
      return Object.keys(this.props.fields).reduce((changed, name) => {
         const field = this.props.fields[name];
         const type = field.type in this.compareHandlers ? field.type : 'default';

         if (!this.compareHandlers[type](name, field)) {
            changed.push(name);
         }

         return changed;
      }, []);
   }

   /**
    * React lifecycle method, initial actions
    */
   componentDidMount() {
      // Set initial form state
      this.setInitialState();

      // If emitter provided, listen and submit form when event fired
      if (this.props.submitEmitter) {
         this.props.submitEmitter.addEventListener('click', () => this.handleFormSubmit());
      }
   }

   /**
    * Sets inital component state based on initial values for fields
    */
   setInitialState() {
      const initialObject = Object.keys(this.props.fields).reduce((map, currName) => {
         const field = this.props.fields[currName];
         const getterValue = field.mobxGetter();

         map[currName] = getterValue ? field.mobxGetter() : '';

         return map;
      }, {});

      this.setState(initialObject);
   }

   /**
    * Render field function, given config returns React component for each field
    *
    * @param {Object} field - Configuration for field
    * @param {String} name - Unique name of field
    * @returns	{Component} - React component
    */
   renderField(field, name) {
      if (field.conditional && !field.conditional(this.state)) {
         return null;
      }

      const Field = typeRenderers[field.type];

      return (
         <FormGroup row key={name}>
            <Label sm={3}>
               {field.title}
               {field.required ? ' *' : ''}
            </Label>
            <Field
               value={this.state[name]}
               field={field}
               name={name}
               setter={val => {
                  this.setState({ [name]: val }, () => {
                     if (field.saveOnChange) {
                        this.submitFormData([ name ]);
                     }
                  });
               }}
               builderState={this.state}
            />
         </FormGroup>
      );
   }

   /**
    * Primary render function, renders form
    *
    * @returns	{Component} - React component for form
    */
   render() {
      const { fields, buttonDisplayText, displaySaveBtn } = this.props;

      return (
         <form
            className={this.state.validated ? 'was-validated ' : ''}
            onSubmit={e => this.handleFormSubmit(e)}
            ref={form => (this.form = form)}
         >
            <Container className={styles.formContainer}>
               {Object.keys(fields).map(name => this.renderField(fields[name], name))}
            </Container>

            {displaySaveBtn && (
               <SaveButton
                  displayText={buttonDisplayText}
                  onSubmitHandler={e => this.handleFormSubmit(e)}
                  enableCondition={!!this.changedFields.length}
               />
            )}
         </form>
      );
   }
}

export default FormBuilder;
